package domain;

public enum DeckStatus {

    Complete,
    Incomplete

}
