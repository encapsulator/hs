package domain;

import static domain.DeckStatus.Incomplete;
import java.util.ArrayList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.OneToMany;
import javax.persistence.Entity;
import javax.validation.Valid;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Deck extends DomainObjectBase {
    @NotEmpty(message="{error.deck.name.null}")
    private String name;
    @NotEmpty(message="{error.deck.playerClass.null}")
    private String playerClass;
    private DeckStatus status = Incomplete;
    @Valid
    @OneToMany(cascade = ALL, mappedBy = "deck")
    private final List<CardReference> cards;

    public Deck(long id) {
        this();
        setId(id);
    }

    public Deck() {
        this.cards = new ArrayList<>();
    }

    public String getPlayerClass() {
        return playerClass;
    }

    public void setPlayerClass(String playerClass) {
        this.playerClass = playerClass;
    }
    

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public DeckStatus getStatus() {
        return status;
    }

    public void setStatus(DeckStatus status) {
        this.status = status;
    }

    public List<CardReference> getCards() {
        return this.cards;
    }

    public void addCard(long cardId) throws DomainException {

        CardReference reference = new CardReference(cardId, this);
        if (cards.contains(reference)) {
            reference = cards.get(cards.indexOf(reference));
            if (reference.getCount() == 1) {
                reference.setCount(2);
            }
        } else {
            cards.add(reference);
        }

    }
    
    public void removeCard(Card card){
        Long cardId = card.getId();
        for(CardReference cardref:cards) {
            if(cardId.equals(cardref.getCardId())) {
                cards.remove(cardref);
                break;
            }
        }
    }

    public int getSize() {
        int size = 0;
        for (CardReference reference : this.cards) {
            size += reference.getCount();
        }
        return size;
    }

}
