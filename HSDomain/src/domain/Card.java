/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;
import javax.persistence.Entity;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;
/**
 *
 * @author niels
 */
@Entity
public class Card extends DomainObjectBase {
    @NotEmpty(message="{error.card.name.null}")
    private String name;
    @NotEmpty(message="{error.card.type.null}")
    private String type;
    @NotEmpty(message="{error.card.text.null}")
    private String text;
    @NotEmpty(message="{error.card.playerClass.null}")
    private String playerClass;
    @Min(value=0,message="{error.card.attack.min}")
    private int attack;
    @Min(value=0,message="{error.card.health.min}")
    private int health;
    @Min(value=0,message="{error.card.cost.min}")
    private int cost;
    
    public Card() {}
    
    public Card(String name, String type, String text, String playerClass, int attack, int health, int cost) {
        this.name = name;
        this.type = type;
        this.text = text;
        this.playerClass = playerClass;
        this.attack = attack;
        this.health = health;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPlayerClass() {
        return playerClass;
    }

    public void setPlayerClass(String playerClass) {
        this.playerClass = playerClass;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost){
        this.cost = cost;
    }
    
}
