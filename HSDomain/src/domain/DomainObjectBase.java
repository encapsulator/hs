/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import javax.persistence.MappedSuperclass;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

/**
 *
 * @author niels
 */
@MappedSuperclass
public abstract class DomainObjectBase implements DomainObject {

    @Id
    @GeneratedValue
    private Long id;

    public DomainObjectBase() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
}
