/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;

/**
 *
 * @author niels
 */
public interface DomainObject extends Serializable {
 
    public Long getId() ;

    public void setId(long id);
    
}
