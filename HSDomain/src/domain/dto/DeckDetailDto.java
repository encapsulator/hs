/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain.dto;

/**
 *
 * @author niels
 */
public class DeckDetailDto {
    private String name;
    private int count;
    private long id;
    private String playerClass;
    
    public DeckDetailDto() {}
    public DeckDetailDto(String name, int count) {
        this.name = name;
        this.count = count;
    }
    
    public DeckDetailDto(String name, int count, long id) {
        this(name, count);
        this.id = id;
    }
    
    public DeckDetailDto(String name, int count, long id, String playerClass) {
        this(name, count, id);
        this.playerClass = playerClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlayerClass() {
        return playerClass;
    }

    public void setPlayerClass(String playerClass) {
        this.playerClass = playerClass;
    }
    
}
