/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.memory;

import db.MapperLocator;

/**
 *
 * @author niels
 */
public class MemoryMapperLocator extends MapperLocator {
    
    public MemoryMapperLocator() {
        cardMapper = new MemoryCardMapper();
        deckMapper = new MemoryDeckMapper();
    }
    
}
