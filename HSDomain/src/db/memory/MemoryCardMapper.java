/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.memory;

import db.Mapper;
import db.MapperException;
import domain.Card;
import domain.DomainException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author niels
 */
public class MemoryCardMapper implements Mapper<Card> {

    private Map<Long, Card> cards;
    private int id;
    
    public MemoryCardMapper() {
        cards = new HashMap<>();
        try {
            Card ragnaros = new Card("Ragnaros", "Minion", "Can't Attack. At the end of turn deal 8 damage to a random enemy character", "All classes", 8, 8, 8);
            ragnaros.setId((long)1);
            cards.put(ragnaros.getId(), ragnaros);
            id = 1;
        } catch (DomainException ex) {
            Logger.getLogger(MemoryCardMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public long store(Card domainObject) throws MapperException {
        domainObject.setId(++id);
        cards.put(domainObject.getId(), domainObject);
        return domainObject.getId();
    }

    @Override
    public void update(Card domainObject) throws MapperException {
        Card card = cards.get(domainObject.getId());
        if(card == null) {
            throw new MapperException("Card with id " + domainObject.getId() + " not found");
        }
        cards.remove(domainObject.getId());
        cards.put(domainObject.getId(), domainObject);
    }

    @Override
    public void delete(long id) throws MapperException {
        Card card = cards.get(id);
        if(card == null) {
            throw new MapperException("Card with id " + id + " not found");
        }
        cards.remove(id);
    }

    @Override
    public Card find(long id) throws MapperException {
        Card card = cards.get(id);
        if(card == null) {
            throw new MapperException("Card with id " + id + " not found");
        }
        return card;
    }

    @Override
    public Collection<Card> findAll() {
        return cards.values();
    }

    @Override
    public void close() throws MapperException {
        cards.clear();
    }
    
    
    @Override
    public void clear() throws MapperException {
        cards.clear();
    }
    
}
