

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational;

import db.Mapper;
import db.MapperException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import domain.DomainObject;
import javax.persistence.Query;


public abstract class RelationalMapper<DO extends DomainObject> implements Mapper<DO> {

    protected final EntityManagerFactory emf = Persistence.createEntityManagerFactory("HearthstonePU");
    private final Class<DO> type;

    public RelationalMapper(Class<DO> type) {
        this.type = type;
    }

    @Override
    public void close() throws MapperException {
        try {
            if (emf != null && emf.isOpen()) {
                emf.close();
            }
        } catch (Exception ex) {
            throw new MapperException(ex);
        }
    }

    @Override
    public long store(DO identifiable) throws MapperException {
        EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(identifiable);
            em.flush();
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception rollback) {
                throw new MapperException(rollback);
            }
            throw new MapperException(ex);
        } finally {
            em.close();
        }
        return identifiable.getId();
    }

    @Override
    public List<DO> findAll() {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<DO> lQuery = em.createQuery("from " + type.getSimpleName() + " t", this.type);
            return lQuery.getResultList();
        } catch (Exception ex) {
            return new LinkedList<>();
        } finally {
            em.close();
        }
    }

    @Override
    public DO find(long id) throws MapperException {
        EntityManager em = emf.createEntityManager();
        try {
            DO result = em.find(type, id);
            if (result == null) {
                throw new MapperException("Id: '" + id + "'");
            }
            return result;
        } catch (MapperException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new MapperException(ex);
        } finally {
            em.close();
 
        }
    }

    @Override
    public void update(DO domainObject) throws MapperException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            DO persisted = em.find(type, domainObject.getId());
            if (persisted == null) {
                throw new MapperException("Id: '" + domainObject.getId() + "'");
            }
            em.merge(domainObject);
            em.flush();
            em.getTransaction().commit();
        } catch (MapperException ex) {
            throw ex;
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception rollback) {
                throw new MapperException(rollback);
            }
            throw new MapperException(ex);
        } finally {
            em.close();
        }
    }

    @Override
    public void delete(long id) throws MapperException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            DO persisted = em.find(type, id);
            if (persisted == null) {
                throw new MapperException("Id: '" + id + "'");
            }
            em.remove(persisted);
            em.flush();
            em.getTransaction().commit();
        } catch (MapperException ex) {
            throw ex;
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception rollback) {
                throw new MapperException(rollback);
            }
            throw new MapperException(ex);
        } finally {
            em.close();
        }
    }
    
    @Override
    public void clear() throws MapperException {

        EntityManager em = emf.createEntityManager();
        try {
            Query query = em.createQuery("delete from " + type.getSimpleName() + " t");
            query.executeUpdate();
        } catch (Exception ex) {
            throw new MapperException(ex);
        } finally {
            em.close();
        }
       
    }
}

