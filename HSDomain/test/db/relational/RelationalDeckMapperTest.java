/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational;

import db.MapperException;
import domain.Card;
import domain.Deck;
import domain.DomainException;
import static junit.framework.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author niels
 */
public class RelationalDeckMapperTest {

    private static RelationalCardMapper cardMapper;
    private static RelationalDeckMapper deckMapper;
    private Deck deck, persisted;
    private final Card card = new Card("Ragnaros", "Minion", "Can't Attack. At the end of turn deal 8 damage to a random enemy character", "All classes", 8, 8, 8);
    private Long cardId, persistedId;
    public RelationalDeckMapperTest() throws DomainException, MapperException {
        
        this.deck = new Deck();
        deck.setName("Naam");
        deck.setPlayerClass("playerClass");
    }

    @BeforeClass
    public static void setUpClass() throws DomainException {
        deckMapper = new RelationalDeckMapper();
        cardMapper = new RelationalCardMapper();
    }

    @AfterClass
    public static void tearDownClass() throws MapperException {
        deckMapper.close();
        cardMapper.close();
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() throws MapperException {
      
        deckMapper.delete(persisted.getId());
        if(cardId != null) {
            cardMapper.delete(cardId);
        }
        //
    }

    @Test
    public void testStoreDeck_DeckIsPersisted() throws MapperException {
        persistedId = deckMapper.store(deck);
        persisted = deckMapper.find(persistedId);
        assertEquals(deck.getName(), persisted.getName());
    }
    
    @Test
    public void testStoreDeck_DeckWithTwoCards_DeckIsPersisted() throws MapperException, DomainException {
        
        cardId = cardMapper.store(card);
        deck.addCard(cardId);
        deck.addCard(cardId);
        persistedId = deckMapper.store(deck);
        persisted = deckMapper.find(persistedId);
        assertEquals(deck.getName(), persisted.getName());
        assertTrue(deck.getSize() == 2);
    }

}
