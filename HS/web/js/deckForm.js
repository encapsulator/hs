/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cards = [];

$(document).ready(function () {
    $('#cardSelect').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = optionSelected.val();
        var playerClass = $('#deckClass').val();
        //if card has been selected
        if (valueSelected !== 'selectCard') {
            var cardId = optionSelected.data('id');
            var cardClass = optionSelected.data('playerclass');
            //Only add cards that are from the deck's class or all the classes
            if (cardClass === playerClass || cardClass === "All classes") {
                var found = false;
                //check wether we have added the card already, if so, update the table data
                $.each(cards, function (key, value) {
                    var name = value.name;
                    var amount = value.amount;
                    if (valueSelected === name) {
                        found = true;
                        if (amount === 1) {
                            value.amount = 2;
                            //iterate over tabledata
                            $("tr.newRow").each(function () {
                                if ($(this).data('cardname') === valueSelected) {
                                    $(this).find('.cardAmount').html(2);
                                }
                            });
                        }
                    }
                });
                //If we have not yet added the card to our table
                if (found === false) {
                    $("#tableData").append("<tr class='newRow' data-id='" + cardId + "' data-cardname='" + valueSelected + "'><td class='cardName'>" + valueSelected + "</td><td>" + cardClass + "</td>\n\
                    <td><a class='removeCard' href='#'>Remove</a></td><td class='cardAmount'>1</td>\n\
                    </tr>");
                    var card = {
                        "name": valueSelected,
                        "amount": 1
                    };
                    cards.push(card);
                }
            }
        }
    });
    //Remove a card from the table
    $("body").on('click', '.removeCard', function () {
        var cardName = $(this).parent().parent().find(".cardName").html();
        for (var i = 0; i < cards.length; i++) {
            if (cards[i].name === cardName) {
                cards.splice(i, 1);
            }
        }

        $(this).parent().parent().remove();
    });

    //remove table when deck changes playerClass
    $('#deckClass').on('change', function () {
        $("#tableData").html('');
        cards = [];
    });
    //send form, create form elements based on table
    $("#sendForm").on('click', function () {
        $("tr.newRow").each(function (key, value) {
            var amount = $(this).find('.cardAmount').html();
            var cardId = $(this).data('id');
            $("#deckForm").append("<input type='hidden' name='cards[" + key + "].cardId' value='" + cardId + "'/>");
            $("#deckForm").append("<input type='hidden' name='cards[" + key + "].count' value='" + amount + "'/>");
        });
        $("#deckForm").submit();
    });

});

//populatecards if update form
function populateCards() {
    $("tr.newRow").each(function () {
        var cardName = $(this).data('cardname');
        var amount = $(this).find('.cardAmount').html().trim();
        var card = {
            "name": cardName,
            "amount": parseInt(amount)
        };
        cards.push(card);
    });
}
