<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            Hearthstone | 
            <spring:message code="title.addCard">
            </spring:message>
        </title>
        <jsp:include page="title.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="menu.jsp" />
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <form:form action="${pageContext.request.contextPath}/cardForm.htm" method="post" modelAttribute="card">
                                <form:errors path="*" cssClass="alert alert-danger" element="div" />
                                <c:choose>
                                    <c:when test="${card.id > 0}">
                                        <h3><spring:message code="card.update">
                                        </spring:message></h3>
                                        <form:hidden path="id" value="${card.id}"/>
                                        <input type="hidden" name="dispatcher" value="updateCard"/>
                                    </c:when>
                                    <c:otherwise>
                                        <h3><spring:message code="card.add">
                                        </spring:message></h3>
                                        <input type="hidden" name="dispatcher" value="createCard"/>
                                    </c:otherwise>
                                </c:choose>


                                <div class="form-group">
                                    <form:label path="name" value="${param.name}"><spring:message code="card.name">
                                        </spring:message>: </form:label>
                                    <form:input path="name" class="form-control"></form:input>
                                    </div>
                                    <div class="form-group">
                                    <form:label path="type">Type: </form:label>

                                    <form:select class="form-control" path="type">
                                        <form:option value="Minion">Minion</form:option>
                                        <form:option value="Spell">Spell</form:option>
                                        <form:option value="Weapon">Weapon</form:option>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <form:label path="text"><spring:message code="card.text">
                                        </spring:message>: </form:label>
                                    <form:input path="text" class="form-control"></form:input>
                                    </div>
                                    <div class="form-group">
                                    <form:label path="playerClass"><spring:message code="card.playerClass">
                                        </spring:message>: </form:label>
                                    <form:select class="form-control" path="playerClass">
                                        <form:option value="All classes">All classes</form:option>
                                        <form:option value="Priest">Priest</form:option>
                                        <form:option value="Shaman">Shaman</form:option>
                                        <form:option value="Mage">Mage</form:option>
                                        <form:option value="Warrior">Warrior</form:option>
                                        <form:option value="Warlock">Warlock</form:option>
                                        <form:option value="Paladin">Paladin</form:option>
                                        <form:option value="Rogue">Rogue</form:option>
                                        <form:option value="Druid">Druid</form:option>
                                        <form:option value="Hunter">Hunter</form:option>
                                    </form:select>
                                </div>

                                <div class="form-group">
                                    <form:label path="attack"><spring:message code="card.attack">
                                        </spring:message>: </form:label>
                                    <form:input path="attack" class="form-control"></form:input>
                                    </div>
                                    <div class="form-group">
                                    <form:label path="health"><spring:message code="card.health">
                                        </spring:message>: </form:label>
                                    <form:input path="health" class="form-control"></form:input>
                                    </div>
                                    <div class="form-group">
                                    <form:label path="cost"><spring:message code="card.cost">
                                        </spring:message>: </form:label>
                                    <form:input path="cost" class="form-control"></form:input>
                                    </div>
                                    <div class="form-group"><form:button type="submit" class="btn btn-primary"><spring:message code="form.send">
                                        </spring:message></form:button></div>
                            </form:form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->
       <!-- Bootstrap Core JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>
