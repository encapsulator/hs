<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <img src="${pageContext.request.contextPath}/images/logo.png" class="pull-left" id="logo"/>
        <li class="sidebar-brand">

            
            <a id="hoofding" href="${pageContext.request.contextPath}/cards">Hearthstone</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/cards">
                <spring:message code="menu.cards">
                </spring:message>
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/decks">
                <spring:message code="menu.decks">
                </spring:message>
            </a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->
