
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <title>
            Hearthstone | 
            <spring:message code="title.decks">
            </spring:message>
        </title>
        <jsp:include page="title.jsp" />
        <jsp:include page="dataTable.jsp"/>

    </head>

    <body>

        <div id="wrapper">
            <jsp:include page="menu.jsp" />
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="#menu-toggle" class="btn btn-default pull-right" id="menu-toggle">
                                <spring:message code="menu.toggle">
                                </spring:message>
                            </a>
                            <form  method="POST" action="${pageContext.request.contextPath}/lang/">
                                <label for="language">
                                    <spring:message code="language">
                                    </spring:message>
                                </label>
                                <select id="language" name="lang" onchange="submit()">
                                    <option value="en_GB" ${pageContext.response.locale == 'en_GB' ? 'selected' : ''}>English</option>
                                    <option value="nl_BE" ${pageContext.response.locale == 'nl_BE' ? 'selected' : ''}>Nederlands</option>
                                </select>
                            </form>

                            <h1>
                                <spring:message code="title.decks">
                                </spring:message>
                            </h1>
                            <table class="table table-hover" id="table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>
                                            <spring:message code="deck.name">
                                            </spring:message>
                                        </th>
                                        <th>
                                        <spring:message code="deck.playerClass">
                                            </spring:message>
                                        </th>
                                        <th>
                                            Details
                                        </th>
                                        <th>
                                            <spring:message code="deck.update"></spring:message>
                                        </th>
                                        <th>
                                        <spring:message code="deck.remove"></spring:message>
                                        </th>
                                        
                                    </tr>
                                </thead>
                                <tbody id="tableid">
                                    <c:if test="${not empty decks}">
                                        <c:forEach var="deck" items="${decks}">
                                            <tr>
                                                <td>${deck.id}</td>
                                                <td>${deck.name}</td>
                                                <td>${deck.playerClass}</td>
                                                <td>
                                                    <a href="${pageContext.request.contextPath}/decks/${deck.id}/detail" class="pull-right">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </a>
                                                </td>
                                                <td><a href="${pageContext.request.contextPath}/decks/${deck.id}/update" class="pull-right"><span class="glyphicon glyphicon-pencil"></a></td>
                                                <td>
                                                    <a href="${pageContext.request.contextPath}/decks/${deck.id}/remove" class="pull-right">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </tbody>
                            </table>
                                        <a href="${pageContext.request.contextPath}/addDeck">
                                        <spring:message code="deck.add"></spring:message>
                                    </a>
                        </div>
                       
                    </div>
                </div>

            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Bootstrap Core JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
                                    $("#menu-toggle").click(function (e) {
                                        e.preventDefault();
                                        $("#wrapper").toggleClass("toggled");
                                    });
        </script>

    </body>

</html>