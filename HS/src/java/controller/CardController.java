/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.MapperException;
import domain.Card;
import domain.Deck;
import domain.DomainException;
import domain.HearthstoneService;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author niels
 */
@Controller
public class CardController {

    private HearthstoneService service;

    public HearthstoneService getService() {
        return service;
    }

    @Autowired
    public void setService(HearthstoneService service) {
        this.service = service;
    }

    @RequestMapping(value = "/cards", method = GET)
    public ModelAndView getCards() throws DomainException, MapperException {
        Collection<Card> cards = service.getCards();
        return new ModelAndView("cards", "cards", cards);
    }

    @RequestMapping(value = "/updateCard", method = GET)
    public ModelAndView updateCard(@RequestParam(value = "cardId") long cardId) {
        Card card = null;
        try {
            card = service.getCard(cardId);
        } catch (MapperException ex) {
            Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ModelAndView("cardForm", "card", card);
    }

    @RequestMapping(value = {"/cardForm"}, method = POST, params = {"dispatcher=updateCard"})
    public ModelAndView updateCard(@Valid @ModelAttribute("card") Card card, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("cardForm");
        } else {
            try {
                service.updateCard(card);
            } catch (MapperException ex) {
                Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return new ModelAndView("redirect:/");
        }
    }

    //@RequestParam long cardId
    @RequestMapping(value = "/removeCard")
    public String removeCard(@RequestParam(value = "cardId") long cardId) {
        try {
            deleteCardInDecks(service.getCard(cardId));
            service.removeCard(cardId);
        } catch (MapperException ex) {
            Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/addCard", method = GET)
    public ModelAndView addCard() {
        Card card = new Card();
        return new ModelAndView("cardForm", "card", card);
    }

    @RequestMapping(value = {"/cardForm"}, method = POST, params = {"dispatcher=createCard"})
    public ModelAndView addCard(@Valid @ModelAttribute("card") Card card, BindingResult result) {
        System.out.println(result.getErrorCount());
        if (result.hasErrors()) {
            return new ModelAndView("cardForm");
        } else {
            try {
                service.addCard(card);
            } catch (MapperException ex) {
                Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return new ModelAndView("redirect:/");
        }
    }

    /* --- Locale --- */
    @RequestMapping(value = "/setLocale")
    public String setLocale(HttpServletRequest request) {
        return "redirect:" + request.getHeader("Referer");
    }

    private void deleteCardInDecks(Card card) {
        for (Deck deck : service.getDecks()) {
            deck.removeCard(card);
            try {
                service.updateDeck(deck);
            } catch (MapperException ex) {
                Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
