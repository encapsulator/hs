/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.MapperException;
import domain.Card;
import domain.CardReference;
import domain.Deck;
import domain.DomainException;
import domain.HearthstoneService;
import domain.dto.DeckDetailDto;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author niels
 */
@Controller
public class DeckController {

    private HearthstoneService service;

    public HearthstoneService getService() {
        return service;
    }

    @Autowired
    public void setService(HearthstoneService service) {
        this.service = service;
    }

    @RequestMapping(value = "/decks", method = GET)
    public ModelAndView getDecks() throws DomainException, MapperException {

        Collection<Deck> decks = service.getDecks();
        return new ModelAndView("decks", "decks", decks);
    }

    //@RequestParam long cardId
    @RequestMapping(value = "/removeDeck")
    public ModelAndView removeDeck(@RequestParam(value = "deckId") long deckId, HttpServletRequest request) {
        try {
            service.removeDeck(deckId);
        } catch (MapperException ex) {
            Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ModelAndView("decks", "decks", service.getDecks());
    }

    @RequestMapping(value = "/deckDetail")
    public ModelAndView deckDetail(@RequestParam(value = "deckId") long deckId) {
        Map<String, Object> params = new HashMap<>();

        Deck deck = null;
        try {
            deck = service.getDeck(deckId);
        } catch (MapperException ex) {
            Logger.getLogger(DeckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ModelAndView mav = new ModelAndView("deckDetail");
        mav.addObject("name", deck.getName());
        mav.addObject("playerClass", deck.getPlayerClass());
        mav.addObject("cards", getCardsDtoDeck(deck));
        return mav;
    }

    @RequestMapping(value = "/addDeck", method = GET)
    public ModelAndView addDeck() {
        ModelAndView mav = new ModelAndView("deckForm");
        mav.addObject("deck", new Deck());
        mav.addObject("cards", service.getCards());
        return mav;
    }

    @RequestMapping(value = {"/deckForm"}, method = POST, params = {"dispatcher=createDeck"})
    public ModelAndView addDeck(@Valid @ModelAttribute("deck") Deck deck, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("deckForm", "cards", service.getCards());
        } else {
            try {
                service.addDeck(deck);
            } catch (MapperException ex) {
                Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return new ModelAndView("decks", "decks", service.getDecks());
        }
    }

    @RequestMapping(value = "/updateDeck", method = GET)
    public ModelAndView updateDeck(@RequestParam(value = "deckId") long deckId) {
        ModelAndView mav = new ModelAndView("deckForm");
        Deck deck = null;
        try {
            deck = service.getDeck(deckId);
        } catch (MapperException ex) {
            Logger.getLogger(CardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        mav.addObject("deck", deck);
        mav.addObject("cards", service.getCards());
        mav.addObject("existingCards", getCardsDtoDeck(deck));
        return mav;
    }

    private List<DeckDetailDto> getCardsDtoDeck(Deck deck) {
        List<DeckDetailDto> cards = new ArrayList<>();
        for (CardReference reference : deck.getCards()) {
            long id = reference.getCardId();
            int count = reference.getCount();
            Card card = null;
            try {
                card = service.getCard(id);
            } catch (MapperException ex) {
                Logger.getLogger(DeckController.class.getName()).log(Level.SEVERE, null, ex);
            }
            cards.add(new DeckDetailDto(card.getName(), count, card.getId(), card.getPlayerClass()));
        }
        return cards;
    }

    @RequestMapping(value = {"/deckForm"}, method = POST, params = {"dispatcher=updateDeck"})
    public ModelAndView updateDeck(@Valid @ModelAttribute("deck") Deck deck, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("deckForm");
        } else {
            try {
                service.updateDeck(deck);
            } catch (MapperException ex) {
                Logger.getLogger(DeckController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return new ModelAndView("decks", "decks", service.getDecks());
        }
    }

}
